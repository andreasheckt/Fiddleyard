# Fiddleyard

Motorized fiddleyard for my model train driven by a NEMA stepper, controlled by Arduino

**Used components:**
* Arduino Nano
* Aluminium construction profile 20 x 20 mm with 6 mm groove
* NEMA 14 stepper
* GT2 idler pulley
* 8 mm linear motion rods
* NeoPixels for indicating track information (TODO)
* KY-040 rotary encoder for track selection
* FC-03 Optical end stop switch
* Piezo buzzer for accoustic information
* Emergency stop button