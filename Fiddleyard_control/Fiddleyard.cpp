/*
 * Definition of a motorized fiddleyard
 */

#include "CrossFunc.h"
#include "Fiddleyard.h"
#include <Arduino.h>
#include <AccelStepper.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

// NeoPixels
extern Adafruit_NeoPixel trackLED;
extern uint32_t ledBlack;
extern uint32_t ledOrange;
extern uint32_t ledWhite;

// Stepper
AccelStepper stepper(NEMA_INT_TYPE, NEMA_STEP, NEMA_DIR);

// Constructor
Fiddleyard::Fiddleyard(void) {
}

// Deconstructor
Fiddleyard::~Fiddleyard(void) {
    trackLED.clear();
    trackLED.show();

    #ifdef DEBUG
        Serial.println("Fiddleyard has beed unloaded.");
    #endif
}

// Initialization

// Settings for stepper, switch off NeoPixels and load last track
void Fiddleyard::init() {
    // Read last track number stored in EEPROM and write it to trackAct
    trackAct = int(0 + EEPROM.read(ADDR_TRK));
    if (trackAct == 255) {
        // Initial use
        trackAct = 0;
        EEPROM.update(ADDR_TRK, trackAct);

        #ifdef DEBUG
            Serial.println("Initial use, track No. is set to 0.");
        #endif
    }
    else {
        #ifdef DEBUG
            Serial.println("Last track No. read from EEPROM is " + String(trackAct) + ".");
        #endif
    }

    // NeoPixels
    trackLED.begin();
    trackLED.clear();
    // trackLED.fill(ledBlack, 0, LED_COUNT);
    trackLED.setBrightness(LED_BRIGHTNESS);
    trackLED.show();

    // Stepper
    stepper.setAcceleration(NEMA_ACC);
    stepper.setEnablePin(NEMA_ENABLE);
    stepper.setPinsInverted(false, true, true);
    stepper.setMaxSpeed(NEMA_MAX_SPEED);
    stepper.setCurrentPosition(trackToSteps(trackAct));

    // I want to turn on the stepper driver if necessary
    if (!NEMA_ON_OFF) {
        stepper.enableOutputs();
    }


    #ifdef DEBUG
        Serial.println("Current stepper position: " + String(stepper.currentPosition()));
    #endif

    // I want to make the actual track visible by the NeoPixels
    showTrackAct();

    #ifdef DEBUG
        Serial.println("Initialization finished.");
    #endif
}


// Positioning

// Calculate distance in mm from steps
double Fiddleyard::stepsToMM(long steps) {
    double nemaRevSteps = NEMA_REV_STEPS;   // Typecast to double is necessary to avoid unwanted rounding
    return min(steps / nemaRevSteps * NEMA_REV_MM, maxMM()); // Maximum valid value
}

// Calculate steps from distance in mm
unsigned long Fiddleyard::mmToSteps(double mm) {
    double nemaRevMM = NEMA_REV_MM;   // Typecast to double is necessary to avoid unwanted rounding
    return min(mm / nemaRevMM * NEMA_REV_STEPS, maxSteps()); // Maximum valid value
}

// Calculate track No. from steps
unsigned int Fiddleyard::stepsToTrack(long steps) {
    double nemaRevSteps = NEMA_REV_STEPS;   // Typecast to double is necessary to avoid unwanted rounding
    return min(round(max(steps, 0) / nemaRevSteps * NEMA_REV_MM / TRK_DIST), TRK_NO - 1); // Maximum valid value
}

// Calculate steps from track No.
unsigned long Fiddleyard::trackToSteps(unsigned int track) {
    double nemaRevMM = NEMA_REV_MM;   // Typecast to double is necessary to avoid unwanted rounding
    return min(track * TRK_DIST / nemaRevMM * NEMA_REV_STEPS, maxSteps()); // Maximum valid value
}

// Maximum valid distance in steps
unsigned int Fiddleyard::maxSteps() {
    double nemaRevMM = NEMA_REV_MM;   // Typecast to double is necessary to avoid unwanted rounding
    return (TRK_NO - 1) * TRK_DIST / nemaRevMM * NEMA_REV_STEPS;
}

// Maximum valid distance in mm
double Fiddleyard::maxMM() {
    return (TRK_NO - 1) * TRK_DIST;
}

// Calibrate position using the optical end stop switch
void Fiddleyard::calibrate() {
    int steps = 0;
    unsigned int mmMoveBack = 10; // Maximum way back from closed end stop switch

    // I want to turn on the stepper driver if necessary
    if (NEMA_ON_OFF) {
        stepper.enableOutputs();
    }

    // I want to move the fiddleyard until the end stop switch is closed
    stepper.setCurrentPosition(0);
    stepper.setMaxSpeed(800);
    do {
        stepper.move(-mmToSteps(10));
        if (abs(stepper.currentPosition()) > (maxSteps() + mmToSteps(5))) {
            // Moved to many steps
            // stepper.disableOutputs();
            stepper.stop();
            errorHandling("Fiddleyard has been moved forward more than " + String(stepsToMM(maxSteps())) + " mm and end stop switch is still not triggered – please check hardware!");
        }

        stepper.run();
    } while (digitalRead(ENDS0_DATA) == LOW);
    steps += abs(stepper.currentPosition());

    // I want to move the fiddleyard back until the end stop switch is opened
    stepper.setCurrentPosition(0);
    stepper.setMaxSpeed(400);
    do {
        stepper.move(mmToSteps(mmMoveBack));
        if (stepper.currentPosition() > mmToSteps(mmMoveBack)) {
            // Moved to many steps
            // stepper.disableOutputs();
            stepper.stop();
            errorHandling("Fiddleyard has been moved back more than " + String(mmMoveBack) + " mm and end stop switch is still triggered – please check hardware!");
        }

        stepper.run();
    } while (digitalRead(ENDS0_DATA) == HIGH);
    steps -= abs(stepper.currentPosition());

    // I want to tell the driver the 0-position of the fiddleyard
    stepper.setCurrentPosition(0);
    stepper.setMaxSpeed(NEMA_MAX_SPEED);

    // I want to turn off the stepper driver if necessary
    if (NEMA_ON_OFF) {
        stepper.disableOutputs();
    }

    // I want to indicate the ending of calibration by the piezo buzzer
    for (int i = 0; i < 2; i++) {
        tone(BUZZ_DATA, BUZZ_FREQ);
        delay(100);
        noTone(BUZZ_DATA);
        delay(100);
    }

    #ifdef DEBUG
        Serial.println(String(steps) + " steps corresponding " + String(stepsToMM(steps)) + " mm have been made from the origin position.");
        Serial.println("Fiddleyard has been calibrated using the optical end stop switch near track 0.");
        Serial.println("Fiddleyard now will be moved to track No. " + String(stepsToTrack(steps)) + ".");
    #endif

    // I want to move the fiddleyard to the calculated track position
    moveToTrack(stepsToTrack(steps));
}

// Move fiddleyard to track
void Fiddleyard::moveToTrack(unsigned int track) {
    // I want to switch off the NeoPixels for the actual track No.
    trackLED.setPixelColor(trackAct, ledBlack);
    trackLED.setPixelColor(TRK_NO * 2 - trackAct - 1, ledBlack);
    trackLED.show();

    #ifdef DEBUG
        Serial.println("NeoPixels for track No. " + String(trackAct) + " have been switched off.");
    #endif

    // I want to turn on the stepper driver if necessary
    if (NEMA_ON_OFF) {
        stepper.enableOutputs();
    }

    // I want the stepper to run to the needed
    stepper.runToNewPosition(trackToSteps(track));

    // I want to turn off the stepper driver if necessary
    if (NEMA_ON_OFF) {
        stepper.disableOutputs();
    }

    // I want to write the actual position to the EEPROM
    EEPROM.update(ADDR_TRK, track);
    trackAct = track;

    #ifdef DEBUG
        Serial.println("Fiddleyard has been moved to track No. " + String(track) + ".");
    #endif

    // I want to make the actual track visible by the NeoPixels
    showTrackAct();
}

// Emergency stop
void Fiddleyard::stop() {
    stepper.stop();
    errorHandling("Fiddleyard has been stopped by the emergency button!");
}

// Make the actual track visible by the NeoPixels
void Fiddleyard::showTrackAct() {
    trackLED.setPixelColor(trackAct, ledWhite);
    trackLED.setPixelColor(TRK_NO * 2 - trackAct - 1, ledWhite);
    trackLED.show();

    #ifdef DEBUG
        Serial.println("NeoPixels for track No. " + String(trackAct) + " have been switched to white.");
    #endif
}

// Make the selected track visible by the NeoPixels
void Fiddleyard::showTrackSel(unsigned int track) {
    // I do not want to change the color if the selectet track is the actual track
    if (track != trackAct) {
        trackLED.setPixelColor(track, ledOrange);
        trackLED.setPixelColor(TRK_NO * 2 - track - 1, ledOrange);
        trackLED.show();
    }

    // I want to switch off the not needed NeoPixels
    for (unsigned int trackTmp = 0; trackTmp < TRK_NO; trackTmp++) {
        if (trackTmp != trackAct && trackTmp != track && trackLED.getPixelColor(trackTmp) == ledOrange) {
            trackLED.setPixelColor(trackTmp, ledBlack);
            trackLED.setPixelColor(TRK_NO * 2 - trackTmp - 1, ledBlack);
            trackLED.show();

            #ifdef DEBUG
                Serial.println("NeoPixels for track No. " + String(trackTmp) + " have been switched off.");
            #endif
        }
    }

    #ifdef DEBUG
        Serial.println("NeoPixels for track No. " + String(track) + " have been switched to orange.");
    #endif
}

// Get actual track number
unsigned int Fiddleyard::getTrackAct() {
    return this->trackAct;
}