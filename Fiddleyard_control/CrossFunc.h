/*
 * Declaration of cross functional services
 */

#ifndef _CROSS_FUNC_H_
#define _CROSS_FUNC_H_

#include <Arduino.h>

// Turn on serial output for debuging
#define DEBUG

// Software
#define SW_RELEASE_YEAR  2021               // Software release year
#define SW_LICENSE   "GPL v3"               // Software licence model
#define SW_VERSION     "1.01"               // Software version

// GPIO mapping

/*
 * GPIO  2 --> Input/Output D2, Interrrupt
 * GPIO  3 --> Input/Output D3, Interrrupt
 * GPIO  4 --> Input/Output D4
 * GPIO  5 --> Input/Output D5
 * GPIO  6 --> Input/Output D6
 * GPIO  7 --> Input/Output D7
 * GPIO  8 --> Input/Output D8
 * GPIO  9 --> Input/Output D9
 * GPIO 10 --> Input/Output D10
 * GPIO 11 --> Input/Output D11
 * GPIO 12 --> Input/Output D12
 * GPIO 13 --> Input/Output D13
 * GPIO 14 --> ADC0 A0
 * GPIO 15 --> ADC0 A1
 * GPIO 16 --> ADC0 A2
 * GPIO 17 --> ADC0 A3
 * GPIO 18 --> ADC0 A4
 * GPIO 19 --> ADC0 A5
 
 * GPIO ?? --> ADC6 A6
 * GPIO ?? --> ADC7 A7
 * GPIO ?? --> RESET
 * GPIO ?? --> RX
 * GPIO ?? --> TX
 */

// EEPROM
#define ADDR_TRK            0               // Address to store track number

// Optical end stop switch
#define ENDS0_DATA          3               // Data from end stop switch near track 0

// Emergency stop button
#define ESTOP               2               // Data from emergency stop button

// Rotary encoder
#define ENC_CLK            10               // Clock line from rotary encoder CLK
#define ENC_DT             11               // Data line from rotary encoder DT
#define ENC_BTN            12               // Switch from rotary encoder SW

// Piezo buzzer
#define BUZZ_DATA          13               // Data
#define BUZZ_FREQ        3000               // Frequency

// NeoPixels
#define LED_DATA            6               // Data
#define LED_BRIGHTNESS    255               // Brightness
#define LED_COUNT          10               // 5 tracks with 1 NeoPixel at each end
/*
 * Positions of the NeoPixels on the fiddleyard:
 *
 * ┌────────────────────┐
 * │= 4 ============ 5 =│
 * │= 3 ============ 6 =│
 * │= 2 ============ 7 =│
 * │= 1 ============ 8 =│
 * │= 0 ============ 9 =│
 * └────────────────────┘
 */

// Stepper
#define NEMA_INT_TYPE       1               // Type of interface; 1 = EasyDriver
#define NEMA_DIR            8               // Direction
#define NEMA_ENABLE         7               // Switching driver on/off
#define NEMA_STEP           9               // Step impulse
#define NEMA_ACC          500               // Acceleration
#define NEMA_MAX_SPEED   2000               // Maximum speed; maximum reliably supported speed is 4000 steps per second
#define NEMA_REV_STEPS   3200               // 1 turn = 3.200 * 1/8 steps
#define NEMA_REV_MM        40               // 1 turn = 40 mm distance; 1 turn * 20 teeth * 2 mm per tooth
#define NEMA_ON_OFF     false               // Enabling before and disenabling stepper after movement

// Error handling
void errorHandling(String errorMsg);

// Reset
void softwareReset();

#endif