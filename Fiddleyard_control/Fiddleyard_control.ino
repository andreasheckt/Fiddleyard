/*
 * Copyright (c) 2021 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * --
 *
 * Control for a motorized fiddleyard driven by a NEMA 14 stepper
 * 
 * Valid track numbers: from 0 to number of tracks - 1
 * 
 * Angle made by 1 step: 0.9 dregrees
 * Number of teeth of the driver pulley: 20
 * Teeth distance of pulley: 2 mm
 * 
 * Encoder code found on https://forum.arduino.cc/index.php?topic=213837.0
 */

#include "CrossFunc.h"
#include "Fiddleyard.h"

// Fiddleyard
Fiddleyard yard;

// Rotary encoder
unsigned int trackAct = 0; // Actual track No.
unsigned int trackSel = 0; // Selected track No.
unsigned int trackSelLast = 0; // Last selected track No.
unsigned int trackSelEncoder = 0; // Selected track No. * 4 (for encoder data only)

// Booting sequence
void setup() {
  // Start serial communication
  Serial.begin(115200);
  #ifdef DEBUG
    Serial.println("--->\nStart\n--");
    Serial.println("Fiddleyard control - A project by andreasheckt\nV " + String(SW_VERSION) + "\n(c) " + String(SW_RELEASE_YEAR) + "\n" + String(SW_LICENSE) + "\n");
  #endif

  // Optical end stop switch
  pinMode(ENDS0_DATA, INPUT);

  // Emergency stop button
  pinMode(ESTOP, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ESTOP), emergencyStop, FALLING);

  // Rotary encoder
  pinMode(ENC_BTN, INPUT);
  pinMode(ENC_CLK, INPUT);
  pinMode(ENC_DT, INPUT);
  encImpulse(); // Initialize encoder

  // Piezo buzzer
  pinMode(BUZZ_DATA, OUTPUT);
  tone(BUZZ_DATA, BUZZ_FREQ);
  delay(100);
  noTone(BUZZ_DATA);
  delay(500);

  // NeoPixels
  pinMode(LED_DATA, OUTPUT);

  // NEMA Stepper
  pinMode(NEMA_DIR, OUTPUT);
  pinMode(NEMA_ENABLE, OUTPUT);
  pinMode(NEMA_STEP, OUTPUT);  

  // Fiddleyard
  yard.init();
  // yard.calibrate();
  trackAct = yard.getTrackAct();
  trackSel = yard.getTrackAct();
  trackSelLast = trackSel;
  trackSelEncoder = trackSel * 4;
}

void loop() {
  // I want to know, if there is a new selected track No.
  static unsigned long lastDisplayTime = 0;
  unsigned int trackSel = encTrackSel();

  if (trackSel != trackSelLast && micros() - lastDisplayTime > 2000) {
    // A new track No. has been selected
    trackSelLast = trackSel;
    yard.showTrackSel(trackSel);
    lastDisplayTime = micros();

    #ifdef DEBUG
      Serial.println("Selected Track No.: " + String(trackSel));
    #endif
  }

  // I want to move the fiddleyard to the selected track No.
  if (digitalRead(ENC_BTN) == LOW && trackSel != yard.getTrackAct()) {
    yard.moveToTrack(trackSel);
  }
}

int encImpulse() {
  /*
   * Source: https://forum.arduino.cc/index.php?topic=213837.0
   * Modification: Errors are ignored
   */
  static byte state = 0;
  state = state << 2;
  if (digitalRead(ENC_CLK)) bitSet(state, 1);
  if (digitalRead(ENC_DT)) bitSet(state, 0);
  state = state & 0xF;
  switch (state) {
    case 0b0001:
    case 0b0111:
    case 0b1110:
    case 0b1000: return -1; // left impulse at inputs
    case 0b0010:
    case 0b1011:
    case 0b1101:
    case 0b0100: return 1; // right impuse at inputs
    default: return 0;
  } 
}

// Read selected Track from Encoder
unsigned int encTrackSel() {
  /*
   * Source: https://forum.arduino.cc/index.php?topic=213837.0
   * Modification: Errors are ignored, limited value range
   */
  int thisImpulse = encImpulse(); // Value read from rotary encoder

  if (trackSelEncoder > 0 && thisImpulse < 0) trackSelEncoder += thisImpulse; // Limitation: Minimum value = 0
  if (trackSelEncoder < (TRK_NO - 1) * 4 && thisImpulse > 0) trackSelEncoder += thisImpulse; // Limitation: Maximum value = TRK_NO - 1
  return trackSelEncoder / 4;
}

// Emergency stop
void emergencyStop() {
  if (!digitalRead(ESTOP)) {
    yard.stop();
  }
}