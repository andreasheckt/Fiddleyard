/*
 * Declaration of a motorized fiddleyard
 */

#ifndef _FIDDLEYARD_H_
#define _FIDDLEYARD_H_

// Tracks
#define TRK_NO              5               // Number of tracks
#define TRK_DIST         54.0               // Track distance [mm]

class Fiddleyard {
    private:
        // Positioning

        // Valid track numbers: from 0 to number of tracks - 1
        unsigned int trackAct;              // Actual track No.
        unsigned int trackDest;             // Destination track No.
        unsigned int trackSel;              // Selected track No.
        double stepsToMM(long steps);
                                            // Calculate distance in mm from steps
        unsigned long mmToSteps(double mm);
                                            // Calculate steps from distance in mm
        unsigned int stepsToTrack(long steps);
                                            // Calculate track No. from steps
        unsigned long trackToSteps(unsigned int track);
                                            // Calculate steps from track No.
        unsigned int maxSteps();            // Maximum valid distance in steps
        double maxMM();                     // Maximum valid distance in mm

    public:
        // Constructor
        Fiddleyard(void);

        // Deconstructor
        ~Fiddleyard(void);

        // Initialization
        void init();                        // Settings for stepper, switch off NeoPixels and load last track

        // Positioning
        void calibrate();                   // Calibrate position using the optical end stop switch
        void moveToTrack(unsigned int track);
                                                    // Move fiddleyard to track
        void stop();                        // Emergency stop
        void showTrackAct();                // Make the actual track visible by the NeoPixels
        void showTrackSel(unsigned int track);
                                            // Make the selected track visible by the NeoPixels
        unsigned int getTrackAct();         // Get actual track number
};
#endif