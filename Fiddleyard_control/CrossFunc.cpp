/*
 * Definition of cross functional services
 */

#include "CrossFunc.h"
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

// NeoPixels
/*
 * Source: https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use
 * 
 * Argument 1 = Number of NeoPixels
 * Argument 2 = Arduino pin number (most are valid)
 * Argument 3 = Pixel type flags, add together as needed:
 *   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
 *   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
 *   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
 *   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
 *   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
 */
Adafruit_NeoPixel trackLED(LED_COUNT, LED_DATA, NEO_GRB + NEO_KHZ800);
uint32_t ledBlack = trackLED.Color(0, 0, 0);
                                            // Color information for the NeoPixels
uint32_t ledOrange = trackLED.Color(255, 155, 0);
                                            // Color information for the NeoPixels
uint32_t ledRed = trackLED.Color(255, 0, 0);
                                            // Color information for the NeoPixels
uint32_t ledWhite = trackLED.Color(255, 255, 255);
                                            // Color information for the NeoPixels

// Error handling
void errorHandling(String errorMsg) {
    /*
    * Print error message to serial output in debug mode
    * Flash NeoPixels in ledOrange color
    */
    static unsigned long startTime = millis();// Time when errorHandling begins
    unsigned long durance = 5000;             // Durance for indicating the error by the NeoPixels
    unsigned int ledDelay = 500;              // Delay between flashes

    // I want to disable the emergency stop button
    detachInterrupt(digitalPinToInterrupt(ESTOP));

    #ifdef DEBUG
        Serial.println("\r\n(!)\r\n\r\n" + errorMsg + "\r\n");
    #endif

    // Flash NeoPixels and piezo buzzer
    do {
        // I want to switch the NeoPixels to orange and turn on piezo buzzer
        trackLED.fill(ledRed, 0, LED_COUNT);
        trackLED.show();
        tone(BUZZ_DATA, BUZZ_FREQ);

        // Wait
        delay(ledDelay);

        // I want to switch the NeoPixels to black and turn off piezo buzzer
        trackLED.clear();
        trackLED.show();
        noTone(BUZZ_DATA);

        // Wait
        delay(ledDelay);
    } while ((millis() - startTime) <= durance);

    // I want to loop until the encoder button is pressed longer than 5 senconds
    long duration = 0;
    do {
        // Loop forever
        if (!digitalRead(ENC_BTN)) {
            startTime = millis();
            while (digitalRead(ENC_BTN) == LOW)
            duration = millis() - startTime;
        }
    } while (duration < 5000);

    // I want to reset the Arduino
    softwareReset();
}

// Reset
/*
 * Source: https://forum.arduino.cc/index.php?topic=507800.0
 * Restarts program from beginning but does not reset the peripherals and registers
 */
void softwareReset() {
    asm volatile ("  jmp 0"); 
}